package Integration

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"

	"go.saastack.io/chaku/errors"
	"go.saastack.io/integration/pb"
	"google.golang.org/genproto/protobuf/field_mask"

	events "go.saastack.io/eventspush/pb"

	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/account"
	"go.saastack.io/idutil"

	"go.saastack.io/protos/types"
	"go.saastack.io/userinfo"
	"golang.org/x/oauth2/microsoft"
	"google.golang.org/api/option"

	"google.golang.org/api/calendar/v3"

	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	"go.uber.org/zap"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/golang/protobuf/ptypes/timestamp"
	appTypes "go.saastack.io/app-type/pb"
	billing "go.saastack.io/billing/pb"
	loc "go.saastack.io/location/pb"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	// Generic Error to be returned to client to hide possible sensitive information
	errInternal            = status.Error(codes.Internal, `Oops! Something went wrong`)
	GoogleCalendarScopes   = []string{calendar.CalendarScope}
	OutlookCalendarScopes  = []string{"offline_access", "openid", "profile", "user.read", "calendars.readwrite.shared", "calendars.readwrite"}
	GoogleAnalyticsScopes  = []string{"https://www.googleapis.com/auth/analytics.readonly"}
	GoogleTagManagerScopes = []string{"https://www.googleapis.com/auth/tagmanager.readonly"}
)

const (
	SquareVersion = "2020-03-25"
	RenewError    = "RenewError"
)

type defaultIntegrationServer struct {
	store      pb.IntegrationTokenStore
	locCli     loc.LocationsClient
	info       []*pb.Info
	appTypeCli appTypes.AppTypesClient
	eveCli     events.EventValidatorsClient
	payGwCli   billing.PaymentGatewaySettingsClient
	billAccCli billing.BillingAccountsClient
}

func (s *defaultIntegrationServer) GetIntegrationType(context.Context, *empty.Empty) (*pb.GetIntegrationTypeResponse, error) {
	return &pb.GetIntegrationTypeResponse{
		Type: types.IntegrationType_UNKNOWN_TYPE,
	}, nil
}

// NewIntegrationsServer returns a IntegrationsServer implementation with core business logic
func NewDefaultIntegrationsServer(
	store pb.IntegrationTokenStore,
	info []*pb.Info,
	locCli loc.LocationsClient,
	appTypeCli appTypes.AppTypesClient,
	eveCli events.EventValidatorsClient,
	payGwCli billing.PaymentGatewaySettingsClient,
	billAccCli billing.BillingAccountsClient,
) pb.IntegrationsServer {
	return &defaultIntegrationServer{
		store:      store,
		info:       info,
		locCli:     locCli,
		appTypeCli: appTypeCli,
		payGwCli:   payGwCli,
		eveCli:     eveCli,
		billAccCli: billAccCli,
	}
}

// read info.json file
// parse the above json and get values of specific integration from type field and return the generated url
// also set session so that when user is redirected back session value is verified
// while creating redirect uri attach integration type value in path so that connected knows the integration
func (s *defaultIntegrationServer) Connect(ctx context.Context, in *pb.ConnectRequest) (*pb.ConnectResponse, error) {
	// Check validations
}

// 1. Check state in session, & get user_id
// 2. Check integration type from path variable
// 3. Call access token endpoint and
// 4. Insert data against integration type for that user
func (s *defaultIntegrationServer) Connected(ctx context.Context, in *pb.ConnectedRequest) (*pb.IntegrationToken, error) {
	// Check validations

}
func (s *defaultIntegrationServer) SaveCredential(ctx context.Context, in *pb.SaveCredentialRequest) (*pb.IntegrationToken, error) {

	if in.IntegrationType==types.IntegrationType_Bigcommerce{
		accesstoken,ok:=in.Data["AcessToken"]
		if !ok {
			return nil, status.Error(codes.InvalidArgument, "device-token not found")
		}
		/






	}

	// Check validations
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if in.IntegrationType == types.IntegrationType_ICLOUD {

		const icloudURL = "https://caldav.icloud.com"

		username, ok := in.Data["username"]
		if !ok {
			return nil, status.Error(codes.InvalidArgument, "username not found")
		}

		password, ok := in.Data["password"]
		if !ok {
			return nil, status.Error(codes.InvalidArgument, "password not found")
		}

		// Basic Credential Validation
		re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
		if !re.MatchString(username) || len(password) == 0 {
			return nil, status.Error(codes.InvalidArgument, "invalid credentials passed")
		}

		// Auth Credential Validation
		body := strings.NewReader(`<propfind xmlns='DAV:'><prop><current-user-principal/></prop></propfind>`)
		req, err := http.NewRequest("PROPFIND", icloudURL, body)

		if err != nil {
			return nil, status.Error(codes.Unknown, err.Error())
		}

		// -- Adding Auth Credentials
		req.SetBasicAuth(username, password)
		req.Header.Set("Depth", "0")
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		netClient := &http.Client{
			Timeout: time.Second * 10,
		}

		resp, err := netClient.Do(req)
		if err != nil {
			return nil, status.Error(codes.Unknown, err.Error())
		}
		defer resp.Body.Close()

		if resp.StatusCode == http.StatusUnauthorized {
			return nil, status.Error(codes.InvalidArgument, "invalid credentials passed")
		}

		accessToken := username + ":" + password

		aTEP, err := ptypes.TimestampProto(time.Now().AddDate(20, 0, 0).UTC())
		if err != nil {
			return nil, err
		}

		// New integrationToken info
		it := &pb.IntegrationToken{
			Id:                in.TargetId,
			TargetId:          in.TargetId,
			AccessToken:       accessToken,
			IntegrationType:   in.IntegrationType,
			AccessTokenExpiry: aTEP,
			Active:            true,
			CreatedOn:         ptypes.TimestampNow(),
			CreatedBy:         userinfo.FromContext(ctx).Id,
		}

		// get app-type corresponding to integration
		appType, err := s.appTypeCli.GetAppTypeByIntegrationType(ctx,
			&appTypes.GetAppTypeByIntegrationTypeRequest{
				IntegrationType: in.IntegrationType,
			},
		)
		if err != nil && status.Code(err) != codes.NotFound {
			return nil, err
		}
		it.AppTypeId = appType.GetId()

		// Storing new token info
		ids, err := s.store.CreateIntegrationTokens(ctx, it)
		if err != nil {
			return nil, err
		}
		it.Id = ids[0]

		return it, nil

	} else if in.IntegrationType == types.IntegrationType_FIREBASE {

		deviceToken, ok := in.Data["DeviceToken"]
		if !ok {
			return nil, status.Error(codes.InvalidArgument, "device-token not found")
		}

		deviceId, ok := in.Data["DeviceId"]
		if !ok {
			return nil, status.Error(codes.InvalidArgument, "device-id not found")
		}

		metadata := map[string]string{
			"ModelName":     in.Data["ModelName"],
			"DeviceName":    in.Data["DeviceName"],
			"SystemVersion": in.Data["SystemVersion"],
		}

		token := &pb.IntegrationToken{
			Id:              "",
			TargetId:        in.TargetId,
			IntegrationType: types.IntegrationType_FIREBASE,
			AccessToken:     deviceToken,
			DeviceId:        deviceId,
			Metadata:        metadata,
			Active:          true,
		}

		// get token of user and device
		getToken, err := s.store.GetIntegrationToken(ctx, []string{"id"}, pb.IntegrationTokenAnd{
			pb.IntegrationTokenRevokedEq{Revoked: false},
			pb.IntegrationTokenTargetIdEq{TargetId: in.TargetId},
			pb.IntegrationTokenDeviceIdEq{DeviceId: deviceId},
			pb.IntegrationTokenIntegrationTypeEq{IntegrationType: types.IntegrationType_FIREBASE},
		})
		if err != nil {
			if err == errors.ErrNotFound {
				// create
				id, err := s.store.CreateIntegrationTokens(ctx, token)
				if err != nil {
					return nil, err
				}
				token.Id = id[0]

				return token, nil
			}
			return nil, err
		}

		// update
		token.Id = getToken.Id
		if err := s.store.UpdateIntegrationToken(ctx, token, []string{string(pb.IntegrationToken_AccessToken)}, pb.IntegrationTokenIdEq{Id: token.Id}); err != nil {
			return nil, err
		}
		return token, nil

	} else {
		return nil, status.Error(codes.InvalidArgument, "incorrect integration type")
	}

}

// return integrations list for a user
func (s *defaultIntegrationServer) ListIntegrations(ctx context.Context, in *pb.ListIntegrationsRequest) (*pb.ListIntegrationsResponse, error) {


func (s *defaultIntegrationServer) RevokeTokenInternal(ctx context.Context, in *pb.RevokeTokenRequest) (*empty.Empty, error) {


// expiry token from revoke endpoint of integration
// delete integration entry of that user
// this should not be grpc
func (s *defaultIntegrationServer) RevokeToken(ctx context.Context, in *pb.RevokeTokenRequest) (*empty.Empty, error) {


}


func (s *defaultIntegrationServer) renewToken(ctx context.Context, intG *pb.IntegrationToken) error {


}

func (s *defaultIntegrationServer) RenewToken(ctx context.Context, in *pb.RenewTokenRequest) (*empty.Empty, error) {

}

func (s *defaultIntegrationServer) BatchGetIntegrations(ctx context.Context, req *pb.BatchGetIntegrationsRequest) (*pb.ListIntegrationsResponse, error) {


}
